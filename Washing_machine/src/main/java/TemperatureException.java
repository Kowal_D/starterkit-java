package main.java;

public class TemperatureException extends Exception {

	public TemperatureException() {
	}

	public TemperatureException(String s) {
		super(s);
	}

	public TemperatureException(Throwable cause) {
		super(cause);
	}

	public TemperatureException(String message, Throwable cause) {
		super("B��dna warto��", cause);
	}

}
