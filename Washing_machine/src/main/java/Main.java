package main.java;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {

		Whirlpool wp = new Whirlpool();
		Beko beko = new Beko();
		Amica amica = new Amica();		
		
		System.out.println("Whirlpool:");
		wp.setV(567);
		wp.setProgram(20);
		try {
			wp.setTemperature(46.8);
			beko.setTemperature(46.8);
		} catch (TemperatureException e) {
			e.getMessage();
		}
		wp.showStatus();
		wp.downV();
		wp.nextProgram();
		try {
		wp.tempDown();
		} catch (TemperatureException e) {
			e.getMessage();
		}
		wp.showStatus();
		
		System.out.println("Beko:");
		beko.setV(1000);
		beko.setProgram(3);
		beko.showStatus();
		beko.upV();
		beko.prevProgram();
		try {
		beko.tempUp();
		} catch (TemperatureException e) {
			e.getMessage();
		}
		beko.showStatus();
		
		List<Washer> pralki = new ArrayList<Washer>();
		pralki.add(wp);
		pralki.add(amica);
		pralki.add(beko);
		
		sort(pralki);
	}

	static void sort(List<Washer> list ) {
		System.out.println("\nNieposortowana lista:");
		list.stream().forEach(washer -> System.out.println(washer.getName()));
		
		System.out.println("\nPosortowana lista:");
		list.stream()
		.sorted((i, j) -> i.getName().name().compareTo(j.getName().name()))
		.map(a -> a.getName().name())
		.forEach(System.out::println);
		
		/* inna metoda sortowania:
		List<Washer> newList = list.stream()
				.sorted((i, j) -> i.getName().name().compareTo(j.getName().name()))
				.collect(Collectors.toList());
		newList.stream().forEach(washer -> System.out.println(washer.getName())); 
		 */
	}
}
