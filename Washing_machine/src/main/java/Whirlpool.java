package main.java;

public class Whirlpool extends Washer {
	
	public Whirlpool() {
		super();
		this.setName(WasherBrand.Whirlpool);
		this.setMaxProgram(25);
	}

}
