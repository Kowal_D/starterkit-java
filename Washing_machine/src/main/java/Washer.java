package main.java;

public abstract class Washer {

	private int program;
	private final int minProgram = 1;
	private int maxProgram = 20;
	protected double temperature;
	private final double minTemperature = 0;
	private final double maxTemperature = 90;
	private int v;
	private final int minV = 0;
	private final int maxV = 1000;
	private WasherBrand name;
	
	public Washer() {
		this.program = 1;
		this.temperature = 0;
		this.v = 0;
	}

	public int getProgram() {
		return program;
	}

	public void setProgram(int program) {
		if (this.minProgram<=program && program<=this.maxProgram) {
			this.program = program;
		}
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) throws TemperatureException {
		if (this.minTemperature<=temperature && temperature<=this.maxTemperature) {
			this.temperature = (Math.round(temperature*2.0))/2.0;
			System.out.println("Current temperature: " + this.temperature + " \u00b0C"); //r�wnie� symbol (char)(176)
		}
		else throw new TemperatureException();
	}

	public int getV() {
		return v;
	}

	public void setV(int rotation) {
		if (this.minV<rotation && rotation<=this.maxV)
		this.v = (int)((Math.round((double)(rotation)/100))*100);
	}

	public double getMinTemperature() {
		return minTemperature;
	}


	public double getMaxTemperature() {
		return maxTemperature;
	}


	public int getMinProgram() {
		return minProgram;
	}


	public int getMaxProgram() {
		return maxProgram;
	}

	public void setMaxProgram(int maxProgram) {
		this.maxProgram = maxProgram;
	}

	//od tego miejsca moje funkcje
	
	public void nextProgram() {
		if (this.minProgram<=this.program && this.program<this.maxProgram) {
			this.program+=1;
		}
	}
	
	public void prevProgram() {
		if (this.minProgram<this.program && this.program<=this.maxProgram) {
			this.program-=1;
		}
	}
	
	public void tempUp() throws TemperatureException {
		if (this.minTemperature<=this.temperature && this.temperature<this.maxTemperature) {
			this.temperature+=0.5;
			System.out.println("Current temperature: " + this.temperature + " \u00b0C");
		}
		else throw new TemperatureException();
	}
	
	public void tempDown() throws TemperatureException {
		if (this.minTemperature<this.temperature && this.temperature<=this.maxTemperature) {
			this.temperature-=0.5;
			System.out.println("Current temperature: " + this.temperature + " \u00b0C");
		}
		else throw new TemperatureException();
	}
	
	public void upV() {
		if (this.v == this.maxV) {
			this.v=0;
		}
		else this.v+=100;
	}
	
	public void downV() {
		if (this.v == this.minV) {
			this.v=1000;
		}
		else this.v-=100;
	}
	
	public void showStatus() {
		System.out.println("Program number: " + this.program + " Current temperature: " + this.temperature + " \u00b0C" + " Current speed: " + this.v);
	}
	
	public WasherBrand getName() {
		return name;
	}
	
	public void setName(WasherBrand name) {
		this.name=name;
	}
}
