package main.java;

public class Beko extends Washer {
	
	public Beko() {
		super();
		this.setName(WasherBrand.Beko);
	}

	@Override
	public void setTemperature(double temperature) throws TemperatureException {
		if (this.getMinTemperature()<=temperature && temperature<=this.getMaxTemperature()) {
			this.temperature = (Math.round(temperature));
			System.out.println("Current temperature: " + this.temperature + " \u00b0C");
		}
		else throw new TemperatureException();
	}
	
	@Override
	public void tempUp() throws TemperatureException {
		if (this.getMinTemperature()<=this.temperature && this.temperature<this.getMaxTemperature()) {
			this.temperature+=1;
			System.out.println("Current temperature: " + this.temperature + " \u00b0C");
		}
		else throw new TemperatureException();
	}
	
	@Override
	public void tempDown() throws TemperatureException {
		if (this.getMinTemperature()<this.temperature && this.temperature<=this.getMaxTemperature()) {
			this.temperature-=1;
			System.out.println("Current temperature: " + this.temperature + " \u00b0C");
		}
		else throw new TemperatureException();
	}

}
